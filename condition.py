import steamapi
from home.models import Game, Player


def game_data_check(function):

    def wrapper(*args, **kwargs):
        #print('decorator', steamid)
        response = function(*args, **kwargs)
        games = response['games']
        for game in games:
            if game.header_image == None:
                app_info = steamapi.app_info(game.appid)[str(game.appid)]
                if app_info["success"]:
                    game.header_image = app_info["data"]['header_image']
                    print(game.appid)
                else:
                    # I need fix this
                    game.trash = True
                    pass
                game.save()
        return response

    return wrapper


if __name__ == "__main__":
    my_decorator()