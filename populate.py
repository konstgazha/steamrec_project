import os
os.environ.setdefault('DJANGO_SETTINGS_MODULE',
                      'steamrec.settings')

import django
django.setup()

import requests
from os.path import join
from bs4 import BeautifulSoup
from datetime import datetime
from home.models import Game, Player, PlayerGame, Tag, GameTag
from newgames.models import Pool, RecommendedGame
from steamapi import get_owned_games, group_info, app_info
import numpy as np
from queue import Queue
import threading
import time
from dateutil.parser import parse
from threading import Thread


def threaded(function):
    def decorator(*args, **kwargs):
        t = Thread(target = function, args=args, kwargs=kwargs)
        t.daemon = True
        t.start()
    return decorator


def save_apps():
    url = 'http://api.steampowered.com/ISteamApps/GetAppList/v0002/'
    apps = requests.get(url).json()['applist']['apps']
    games = []
    for app in apps:
        games.append(Game(appid=app['appid'], name=app['name']))
    Game.objects.bulk_create(games)


def collect_apps_name():
    url = 'http://api.steampowered.com/ISteamApps/GetAppList/v0001/'
    apps = requests.get(url).json()['applist']['apps']['app']
    for app in apps:
        game = Game.objects.filter(appid=app['appid'])
        if game:
            game[0].name = app['name']
            game[0].save()
    

def get_steamspy_all_games_info():
    url = 'http://steamspy.com/api.php?request=all'
    apps = requests.get(url).json()
    return apps


def collect_all_games_info(update=True):
    apps = get_steamspy_all_games_info()
    games = Game.objects.all()

    for game in games:
        if game.last_update and (game.last_update - datetime.date(datetime.now())).days < 1:
            continue
        if game.date and (datetime.date(datetime.now()) - game.date).days / 365 > 1:
            continue
        try:
            data = apps[str(game.appid)]
        except KeyError:
            continue
        coming_soon = False
        date = None
        try:
            if int('0' + str(data['score_rank'])) > 47:
                print('collecting', game.appid)
                coming_soon = app_info(game.appid)[str(game.appid)]['data']['release_date']['coming_soon']
                date = parse(app_info(game.appid)[str(game.appid)]['data']['release_date']['date'])
                time.sleep(2)
        except:
            pass
        Game.objects.filter(appid=game.appid).update(name=data['name'].encode('utf-8'),
                                                     average_2weeks=data['average_2weeks'],
                                                     average_forever = data['average_forever'],
                                                     developer = data['developer'].encode('utf-8'),
                                                     publisher = data['publisher'],
                                                     owners = data['owners'],
                                                     score_rank = int('0' + str(data['score_rank'])),
                                                     coming_soon = coming_soon,
                                                     date = date,
                                                     last_update = datetime.now())
        tag_objects = []
        game_tag_objects = []
        for tag in data['tags']:
            try:
                tag_obj = Tag.objects.filter(name=tag)[0]
            except:
                tag_obj = Tag(name=tag)
                tag_objects.append(tag_obj)
            game_tag_obj = GameTag(game=game, tag=tag_obj)
            game_tag_obj.votes = data['tags'][tag]
            game_tag_objects.append(game_tag_obj)
        try:
            Tag.objects.bulk_create(tag_objects)
        except:
            for to in tag_objects:
                try:
                    tag_obj = Tag.objects.filter(name=to.name)[0]
                except:
                    Tag.objects.create(name=to.name)
        try:
            GameTag.objects.bulk_create(game_tag_objects)
        except:
            for gto in game_tag_objects:
                try:
                    game_tag_obj = GameTag.objects.filter(game=game, tag=gto.tag).update(votes=gto.votes)
                except:
                    GameTag.objects.create(game=game, tag=gto.tag, votes=gto.votes)


class PlayerManager:
    def __init__(self, steamid):
        self.steamid = steamid
        self.games = None

    def collect_games_info(self):
        try:
            data = get_owned_games(self.steamid)
            self.games = data["response"]["games"]
            self.game_count = data["response"]["game_count"]
        except:
            #raise "Profile is private"
            return None

    def get_games(self):
        if self.games is None:
            self.collect_games_info()
        return self.games

    def get_top_games(self, N=12):
        if self.games is None:
            self.get_games()
        if self.games:
            playtime_forever = [i['playtime_forever'] for i in self.games]
            if N != 0:
                top_indexes = np.argsort(playtime_forever)[-N:]
            else:
                top_indexes = np.argsort(playtime_forever)
            return [self.games[i] for i in top_indexes]
        else:
            return []

    def save_player(self):
        try:
            Player.objects.create(steamid=self.steamid, last_update = datetime.now())
        except Exception as ex:
            print(ex)

    def get_player_object(self):
        try:
            self.player = Player.objects.filter(steamid=self.steamid)[0]
            return self.player
        except Exception as ex:
            print(ex)
            return None
    
    #@threaded
    def create_player_games(self):
        if self.games is None:
            self.collect_games_info()
        self.save_player()
        player = self.get_player_object()
        player_games = []
        for game in self.games:
            game_obj = Game.objects.get_or_create(appid=game["appid"])[0]
            player_game = PlayerGame(player=player,
                                     game=game_obj,
                                     playtime_forever=game["playtime_forever"])
            if "playtime_2weeks" in game:
                player_game.playtime_2weeks = game["playtime_2weeks"]
            player_games.append(player_game)
        PlayerGame.objects.bulk_create(player_games)
    
    #@threaded
    def update_player_games(self):
        player = self.get_player_object()
        self.player.last_update = datetime.now()
        self.player.save()
        
        if player is None:
            return
        self.collect_games_info()
        if self.games is None:
            player.delete()
            return
        for game in self.games:
            game_obj = Game.objects.get_or_create(appid=game["appid"])[0]
            if "playtime_2weeks" in game:
                pg_obj = PlayerGame.objects.filter(player=player, game=game_obj).update(player=player,
                                                                                        game=game_obj,
                                                                                        playtime_forever=game["playtime_forever"],
                                                                                        playtime_2weeks=game["playtime_2weeks"])
            else:
                pg_obj = PlayerGame.objects.filter(player=player, game=game_obj).update(player=player,
                                                                                        game=game_obj,
                                                                                        playtime_forever=game["playtime_forever"])
            if pg_obj is 0:
                PlayerGame.objects.create(player=player, game=game_obj, playtime_forever=game["playtime_forever"])
        self.player.last_update = datetime.now()
        self.player.save()

    @threaded
    def threaded_update_player_games(self):
        self.update_player_games()
        

@threaded
def save_recommendations(pool_obj, data):
    rec_games = [RecommendedGame(pool=pool_obj, game=i) for i in data['games']]
    RecommendedGame.objects.bulk_create(rec_games)

import statistics
def cluster(data, maxgap):
    data = sorted(data, key=lambda k: k['playtime_forever'])
    groups = [[data[0]]]

    for x in data[1:]:
        if abs(x['playtime_forever'] - statistics.median([i['playtime_forever'] for i in groups[-1]])) <= maxgap:    
            groups[-1].append(x)
        else:
            groups.append([x])
    return groups


def compute_cluster_numers(steamid):
    timestart = time.time()
    x=get_owned_games(steamid)
    x=x["response"]["games"]
    pf=[]
    for i in x:
        pf.append(i['playtime_forever'])
    response_time = time.time() - timestart
    timestart = time.time()
    np.argsort(pf)[-10:]
    clusters = cluster(x, 800)
    cluster_time = time.time() - timestart
    return len(clusters[-1]), response_time, cluster_time

def compute_zero_nonzero_games(steamid):
    response = get_owned_games(steamid)
    response = response["response"]["games"]
    playtime = []
    for game in response:
        playtime.append(game['playtime_forever'])
    nzero = 0
    zero = 0
    for i in playtime:
        if i != 0:
            nzero += 1
        else:
            zero += 1
    return zero, nzero

class SteamGroup:
    def __init__(self, group='steamuniverse'):
        self.group = group

    def get_group_members(self, page):
        members = []
        soup = group_info(self.group, page)
        for row in soup.find_all('steamid64'):
            members.append(row.getText())
        members = list(set(members))
        return members
    
    def collect_group_members(self):
        self.get_group_members()
        players = [Player(steamid=i) for i in self.members]
        try:
            Player.objects.bulk_create(players)
        except:
            print('SQL ERROR: Player.objects.bulk_create(players)')
            pass
        for i, player in enumerate(players):
            print(i)
            player_games = PlayerManager.get_player_games(player.steamid)
            try:
                PlayerGame.objects.bulk_create(player_games)
            except:
                print('SQL ERROR: PlayerGame.objects.bulk_create(player_games)')
                pass


def rebuild_player_database():
    sg = SteamGroup()
    sg.collect_group_members()

