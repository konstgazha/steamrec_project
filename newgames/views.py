from django.shortcuts import redirect, render
from django.views import View
from recommend import ContentFiltering, Collaborative, Filter
from populate import save_recommendations
from .models import Algorithm, Pool, RecommendedGame
from home.models import Game, Player
from random import randint
from datetime import datetime
import numpy as np
import requests
import re
#from home.toolkit import game_data_check

def parse_profile(profile_link):
    if str(profile_link).startswith('7'):
        return str(profile_link)
    url = 'http://api.steampowered.com/ISteamUser/ResolveVanityURL/v0001/?key=EEE24B18F9B4086E59595C6F23E6D1A6&vanityurl=%s'
    if str(profile_link).startswith('http'):
       # try:
        profile_link = re.search('[id|profiles]/(.+)', str(profile_link)).group(0)
        profile_link = profile_link.split('/')[1]
        if str(profile_link).startswith('7'):
            return str(profile_link)
       # except:
       #     pass
    steamid = 0
    try:
        steamid = requests.get(url % (profile_link)).json()
        steamid = steamid['response']['steamid']
    except:
        pass
    print(steamid)
    #raise 'stop'

    return steamid

# Create your views here.
class IndexView(View):
    def __init__(self):
        self.context = {'recommendations': None,
                        'steamid': None,
                        'profile_link': None,
                        'algorithm': ['cb', 'ub', 'ib', 'svd', 'auto'][4], #randint(0, 3)
                        'popular': False,
                        'rating': 0,
                        'pool_id': None}
        self.pool = None

    def get(self, request, *args, **kwargs):
        if 'steamid' in request.session:
            steamid = int(request.session['steamid'])
            self.context['steamid'] = steamid
        if 'algorithm' in request.session:
            self.context['algorithm'] = request.session['algorithm']
        if 'popular' in request.session:
            self.context['popular'] = request.session['popular']
        return render(request, 'newgames/newgames.html', self.context)

    def post(self, request, *args, **kwargs):
        # validate
        if 'steamid' in request.session:
            self.context['steamid'] = request.session['steamid']
        profile = request.POST.get("profile", None)
        cleaned_profile = parse_profile(profile)
        rating_process = request.POST.get("rating_process", None)

        self.context['profile_link'] = profile
        self.context['cleaned_profile'] = cleaned_profile
        self.context['algorithm'] = request.POST.get("algorithm", None)
        self.context['popular'] = request.POST.get("popular", None)

        if self.context['algorithm']:
            request.session['algorithm'] = self.context['algorithm']
        if self.context['popular']:
            request.session['popular'] = self.context['popular']
        steamid = None
        if self.context['steamid']:
            steamid = self.context['steamid'] 
        if self.context['cleaned_profile']:
            steamid = self.context['cleaned_profile']
        
        #rating_process
        if rating_process:
            rating = request.POST.get("rating", None)
            print('RATING:', rating)
            self.context['pool_id'] = request.POST.get("pool_id", None)
            print('ID:', self.context['pool_id'])
            pop = request.POST.get("pop", None)
            if pop == "on":
                pop = 1
            print('POP:', pop)
            try:
                Pool.objects.filter(id=int(self.context['pool_id'])).update(rating=int(rating), popular=pop)
            except Exception as ex:
                print(ex)
            return redirect('/newgames/')

        try:
            int(steamid)
        except:
            del self.context['steamid']
            
            return redirect('/newgames/')
        if steamid:
            #print(steamid)
            #print('pop', self.context['popular']=='on')
            # Luther 76561197998199645
            alg = self.context['algorithm']
            if alg == 'cb':
                recsys = ContentFiltering(steamid=int(steamid),
                                          pop=(self.context['popular']=='on'))
            elif alg == 'ib' or alg == 'ub' or alg == 'svd':
                recsys = Collaborative(steamid=int(steamid),
                                       alg=alg,
                                       pop=(self.context['popular']=='on'))
            else:
                recsys = ContentFiltering(steamid=int(steamid),
                          pop=(self.context['popular']=='on'))
            #print(self.context['algorithm'])
            data = recsys.recommend()
            data['games'] = list(set(data['games']))
            if alg == 'auto':
                recsys = Collaborative(steamid=int(steamid),
                       alg=alg,
                       pop=(self.context['popular']=='on'))
                d = recsys.recommend()
                d['games'] = list(set(d['games']))
                data['games'] = list(set( d['games'] + data['games'] ))
                np.random.shuffle(data['games'])
            self.context['recommendations'] = data['games'][:12]
            try:
                player = Player.objects.get(steamid=data['steamid'])
            except:
                player = Player.objects.create(steamid=data['steamid'])
            try:
                algorithm = Algorithm.objects.get(code_name=alg)
            except:
                algorithm = Algorithm.objects.create(code_name=alg)
            pool_obj = Pool.objects.create(player=player, algorithm=algorithm, date=datetime.now(), rating=0)
            save_recommendations(pool_obj, data)
            self.context['pool_id'] = pool_obj.id
        return render(request, 'newgames/newgames.html', self.context)
