from __future__ import unicode_literals

from django.db import models
from home.models import Game, Player
from datetime import datetime


class Algorithm(models.Model):
    name = models.CharField(max_length=128)
    code_name = models.CharField(max_length=128)


class Pool(models.Model):
    player = models.ForeignKey(Player)
    algorithm = models.ForeignKey(Algorithm)
    date = models.DateField(default=datetime.now())
    rating = models.IntegerField(blank=True, null=True)
    popular = models.IntegerField(default=0)


class RecommendedGame(models.Model):
    pool = models.ForeignKey(Pool)
    game = models.ForeignKey(Game)
    #clicks = models.IntegerField(default=0)