from django.apps import AppConfig


class NewgamesConfig(AppConfig):
    name = 'newgames'
