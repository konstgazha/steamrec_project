from django.conf.urls import url
from django.conf import settings
from django.conf.urls.static import static
from home.views import HomeView, login, logout
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

urlpatterns = [
    url(r'^$', HomeView.as_view(), name='home'),
    #url(r'^logout/', logout, name='logout'),
    #url(r'^login/$', login, name='login'),
    # url(r'^login_process/$', login_process, name='login_process'),
    # url(r'^login_failed/$', views.login_failed, name='login_failed'),
    # url(r'^logout/$', views.logout, name='logout'),
    # url(r'^recommend/$', views.get_recommendation, name='recommend'),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)


urlpatterns += staticfiles_urlpatterns()