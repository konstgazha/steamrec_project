from django.shortcuts import redirect, render
from django.http import HttpResponse
from django.views import View
from steamauth import RedirectToSteamSignIn, GetSteamID64
from random import randint


class HomeView(View):
    def get(self, request, *args, **kwargs):
        context = {'steamid': None}
        if 'steamid' in request.session:
            context = {'steamid': request.session['steamid']}
            print(request.session['steamid'])
        return render(request, 'home_content.html', context)

def login(request):
    if 'steamid' in request.session:
        return redirect('/')
    else:
        if 'login_process' in request.session:
            try:
                request.session['steamid'] = GetSteamID64(request.GET)
                del request.session['login_process']
            except:
                return RedirectToSteamSignIn('/login')
            return redirect('/')
        else:
            request.session['login_process'] = True
        return RedirectToSteamSignIn('/login')

def logout(request):
    request.session.flush()
    return redirect('/')

