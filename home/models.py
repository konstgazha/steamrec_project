from __future__ import unicode_literals

from django.db import models
# from django.contrib.contenttypes.fields import GenericForeignKey
# from django.contrib.contenttypes.models import ContentType


class Game(models.Model):
    # steam api
    appid = models.IntegerField(unique=True)
    name = models.CharField(max_length=128)

    # count of reviews
    recommendations = models.IntegerField(blank=True, null=True)
    coming_soon = models.BooleanField(default=False)
    date = models.DateField(blank=True, null=True)
    header_image = models.URLField(blank=True, null=True)


    # steamspy api
    average_2weeks = models.IntegerField(blank=True, null=True)
    average_forever = models.IntegerField(blank=True, null=True)
    developer = models.CharField(max_length=128)
    publisher = models.CharField(max_length=128)
    owners = models.IntegerField(blank=True, default=0)
    score_rank = models.IntegerField(blank=True, default=0)


    # service
    trash = models.BooleanField(default=False)
    last_update = models.DateField(blank=True, null=True)

    def __str__(self):
        return str(self.appid)


class GamePrice(models.Model):
    game = models.ForeignKey(Game)
    currency = models.CharField(max_length=128)
    # initial price
    initial = models.IntegerField(blank=True, null=True)
    # current price
    final = models.IntegerField(blank=True, null=True)
    discount_percent = models.IntegerField(blank=True, null=True)


# steamspy api
class Tag(models.Model):
    name = models.CharField(max_length=128)


# steamspy api
class GameTag(models.Model):
    game = models.ForeignKey(Game)
    tag = models.ForeignKey(Tag)
    votes = models.IntegerField(blank=True, null=True)
 
    class Meta:
        unique_together = ('game', 'tag')

class Player(models.Model):
    steamid = models.CharField(max_length=128, unique=True)
    newgames_count = models.IntegerField(blank=True, null=True)
    # spendmymoney_count = models.IntegerField(blank=True, null=True)
    game_count = models.IntegerField(blank=True, null=True)
    public = models.BooleanField(default=True)
    avatar = models.CharField(max_length=128)
    avatarmedium = models.CharField(max_length=128)
    avatarfull = models.CharField(max_length=128)

    last_update = models.DateField(blank=True, null=True)

    def __str__(self):
        return str(self.steamid)


class PlayerGame(models.Model):
    player = models.ForeignKey(Player)
    game = models.ForeignKey(Game)
    playtime_forever = models.IntegerField(blank=True, null=True)
    playtime_2weeks = models.IntegerField(blank=True, null=True)
    
    class Meta:
        unique_together = ('player', 'game')

    def __str__(self):
        return str(self.player.steamid) + ' ' + str(self.game.appid)


# class Activity(models.Model):
#     LIKE = 'L'
#     ACTIVITY_TYPES = (
#         (LIKE, 'Like'),
#     )

#     player = models.ForeignKey(Player)
#     activity_type = models.CharField(max_length=1, choices=ACTIVITY_TYPES)
#     date = models.DateTimeField(auto_now_add=True)

#     content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE)
#     object_id = models.PositiveIntegerField()
#     content_object = GenericForeignKey()