import numpy as np
import pandas as pd
from scipy import sparse
import random
import operator
from scipy.spatial.distance import cosine
from scipy.spatial.distance import pdist, squareform
from populate import PlayerManager, threaded, collect_all_games_info
from home.models import Game, Player, PlayerGame, GameTag
import time
from datetime import datetime
import redis


junk = [260190, 359050, 364050, 97330, 207080, 97330, 214730, 273500, 263560,
        449680, 432020, 227100, 49470, 247910, 208500, 6860, 22490, 238050]


class DataFrameManager:
    client = redis.Redis("localhost")
    def get_dataframe():
        timestart = time.time()
        df = DataFrameManager.client.get('dataframe')
        print('LOAD DF ', time.time() - timestart)
        if not df:
            DataFrameManager.set_dataframe()
        else:
            df = pd.read_msgpack(df)
        return df

    def set_dataframe(players_count=1500):
        players = Player.objects.all().values_list('steamid', flat=True)[:players_count]
        pg = []
        for i in players:
            pg += list(PlayerGame.objects.filter(playtime_forever__gte=241, game__score_rank__gte=49, player__steamid=i).exclude(game__date=\
            None).values('player__steamid','game__appid','playtime_forever'))
        df = pd.DataFrame(pg)
        DataFrameManager.client.set('dataframe', df.to_msgpack(compress='zlib'))
        DataFrameManager.client.set('last_update_df', datetime.now().strftime('%Y-%m-%d %H:%M:%S'))

    def get_junk_games():
        games = Game.objects.all().values_list('appid', flat=True)
        gt = []
        for i in games:
            gt += GameTag.objects.filter(game__appid=i).values('votes', 
                                                               'game__appid', 
                                                               'tag__name')
        junk_ids = []
        for i in gt:
            if i['tag__name'] == 'Free to Play' or i['tag__name'] == 'Utilities' \
            or i['tag__name'] == 'Software' or i['tag__name'] == 'Design & Illustration' \
            or i['tag__name'] == 'Movie':
                if i['game__appid'] not in junk_ids:
                    junk_ids.append(i['game__appid'])
        junk_ids += junk
        return junk_ids

    def get_good_games():
        games = DataFrameManager.client.get('good_games')
        if not games:
            junk_ids = DataFrameManager.get_junk_games()
            games = []
            g = Game.objects.all()
            for i in g:
                if i.date != None and i.date > datetime.date(datetime(2011,1,1)) \
                   and i.score_rank > 47 and i.owners >= 500000\
                   and i.appid not in junk_ids:
                    games.append(i.appid)
            DataFrameManager.client.set('good_games', games)
        else:
            games = games.decode('ascii').strip('[]').split(', ')
            games = [int(i) for i in games]
        return games
    
    def get_not_pop_good_games():
        games = DataFrameManager.client.get('not_pop_good_games')
        if not games:
            junk_ids = DataFrameManager.get_junk_games()
            games = []
            g = Game.objects.all()
            for i in g:
                if i.date != None and i.date > datetime.date(datetime(2011,1,1)) \
                   and i.score_rank > 50 and i.owners < 500000 and i.owners > 40000 \
                   and i.appid not in junk_ids:
                    games.append(i.appid)
            DataFrameManager.client.set('not_pop_good_games', games)
        else:
            games = games.decode('ascii').strip('[]').split(', ')
            games = [int(i) for i in games]
        return games

    @threaded
    def threaded_update_df():
        print('UPDATING DF...', datetime.now())
        DataFrameManager.client.set('last_update_df', datetime.now().strftime('%Y-%m-%d %H:%M:%S'))
        steamids = Player.objects.all().values_list('steamid', flat=True)[:1500]
        rnd_users = random.sample(range(1, len(steamids) - 1), 20)
        steamids = [steamids[i] for i in rnd_users]
        for i in steamids:
            print(i)
            player = Player.objects.get(steamid=i)
            if player.last_update:
                if (datetime.date(datetime.now()) - player.last_update).days > 1:
                    PlayerManager(i).update_player_games()
            else:
                player.delete()
        DataFrameManager.set_dataframe()
        print('START COLLECTING ALL GAMES INFO', datetime.now())
        collect_all_games_info()
        print("UPDATING POPULAR GOOD GAMES", datetime.now())
        DataFrameManager.update_pop_good_games()
        print("UPDATING NOT POPULAR GOOD GAMES", datetime.now())
        DataFrameManager.update_not_pop_good_games()
        last_update_cb = DataFrameManager.client.get('last_update_cb')
        if last_update_cb:
            last_update_cb = datetime.strptime(str(last_update_cb).split("'")[1], '%Y-%m-%d %H:%M:%S')
            if (datetime.now() - last_update_cb).total_seconds() > 32400:
                DataFrameManager.threaded_update_cb()
        else:
            DataFrameManager.client.set('last_update_cb', datetime.now().strftime('%Y-%m-%d %H:%M:%S'))

    @threaded
    def update_pop_good_games(min_owners=500000):
            games = []
            g = Game.objects.all()
            for i in g:
                if i.date != None and i.date > datetime.date(datetime(2011,1,1)) \
                   and i.score_rank > 47 and i.owners >= min_owners:
                    games.append(i.appid)
            DataFrameManager.client.set('good_games', games)

    @threaded
    def update_not_pop_good_games(min_owners=100000, max_owners=500000):
            games = []
            g = Game.objects.all()
            for i in g:
                if i.date != None and i.date > datetime.date(datetime(2011,1,1)) \
                   and i.score_rank > 50 and i.owners < max_owners and i.owners > min_owners:
                    games.append(i.appid)
            DataFrameManager.client.set('not_pop_good_games', games)

    def create_content_top_games():
        games = Game.objects.filter(owners__gte=40000, score_rank__gte=10).values_list('appid', flat=True)
        gt = []
        for i in games:
            gt += GameTag.objects.filter(game__appid=i).values('votes', 
                                                               'game__appid', 
                                                               'tag__name')
        df = pd.DataFrame(list(gt))
        unq_games = sorted(list(set(df['game__appid'])))
        unq_tags = sorted(list(set(df['tag__name'])))
        df.set_index(['game__appid', 'tag__name'], inplace=True)
        mat = sparse.coo_matrix((df.votes, (df.index.labels[0], df.index.labels[1])))
        df = pd.DataFrame(mat.toarray(), index=unq_games, columns=unq_tags)
        x = np.array(df)
        idfs = []
        for i in range(x.shape[1]):
            idfs.append(np.log(x.shape[0] / len(x.T[i].nonzero()[0])))
        tfs = np.zeros((x.shape[0], x.shape[1]))
        for i in range(x.shape[0]):
            for j in range(x.shape[1]):
                tfs[i][j] = x[i][j] / np.sum(x[i])
        mtx = np.zeros((x.shape[0], x.shape[1]))
        for i in range(x.shape[0]):
            for j in range(x.shape[1]):
                mtx[i][j] = tfs[i][j] * idfs[j]
        mtx_similarity = squareform(pdist(mtx, 'cosine'))
        result = []
        for i in range(mtx_similarity.shape[0]):
            games = []
            for j in np.argsort(mtx_similarity[i])[1:21]:
                games.append(df.T.columns[j])
            result.append({'appid': df.T.columns[i], 'games': games})
        for i in result:
            DataFrameManager.client.set(i['appid'], i['games'])

    def get_similar_game(appid):
        top_games = DataFrameManager.client.get(appid)
        if DataFrameManager.client.get('content_exists'):
            DataFrameManager.create_content_top_games()
            DataFrameManager.client.set('content_exists', True)
        else:
            top_games = top_games.decode('ascii').strip('[]').split(', ')
            top_games = [int(i) for i in top_games]
        if not top_games:
            print('def get_similar_game >> not top_games')
            return []
        return top_games

    @threaded
    def threaded_update_cb():
        print('UPDATING CONTENT-BASED...', datetime.now())
        DataFrameManager.client.set('last_update_cb', datetime.now().strftime('%Y-%m-%d %H:%M:%S'))
        DataFrameManager.create_content_top_games()


def update_checker(function):
    def decorator(self):
        last_update_df = DataFrameManager.client.get('last_update_df')
        last_update_df = datetime.strptime(str(last_update_df).split("'")[1], '%Y-%m-%d %H:%M:%S')
        if not last_update_df or (datetime.now() - last_update_df).total_seconds() > 14400:
            DataFrameManager.threaded_update_df()

        return function(self)
    return decorator



class RecommenderSystem:
    def __init__(self, steamid, alg='svd', pop=True, K=10, k=10, threshold=101):
        self.steamid = str(steamid)
        self.pop = pop
        self.alg = alg
        self.K = K
        self.k = k
        self.playtime_threshold = threshold
        self.df = None
        self.player_manager = PlayerManager(steamid)
        self.player = self.player_manager.get_player_object()
        pgames = self.player_manager.get_games()
        self.owned_games = []
        for i in pgames:
            self.owned_games.append(i['appid'])
        self.player_top_games, self.player_vector = self.get_player_vector()


    def get_player_vector(self, N=12):
        player_vector = []
        player_top_games = self.player_manager.get_top_games(N)
        #print(player_top_games)
        for i in player_top_games:
            player_vector.append({'game__appid': i['appid'],
                                  'player__steamid': self.steamid,
                                  'playtime_forever': i['playtime_forever']})
        player_vector = pd.DataFrame(player_vector)
        #print(player_vector)
        #raise 'WHAT HTE FCK'
        player_top_games = player_vector['game__appid']
        return player_top_games, player_vector


class ContentFiltering(RecommenderSystem):

    #@update_checker
    def recommend(self, N=20):
        if self.pop:
            good_games = DataFrameManager.get_good_games()
        else:
            not_pop_good_games = DataFrameManager.get_not_pop_good_games()
        games = []
        for i in self.player_top_games:
            try:
                if self.pop:
                    games += Filter.intersect(DataFrameManager.get_similar_game(i), good_games)
                else:
                    games += Filter.intersect(DataFrameManager.get_similar_game(i), not_pop_good_games)
            except Exception as ex:
                print(ex)
                pass
        np.random.shuffle(games)
        print(len(games))
        self.clean_recs = Filter.setdiff(games, self.owned_games)
        game_objects = []
        for i in list(reversed(self.clean_recs[-N:])):
            try:
                game_objects.append(Game.objects.filter(appid=i)[0])
            except:
                #print('Appid does not exists:', i)
                pass
        games = game_objects
        return {'steamid': self.steamid, 'games': games[:12]}


class Collaborative(RecommenderSystem):
    def sparse_matrix(self, df):
        unique_players = sorted(list(set(df['player__steamid'])))
        unique_games = sorted(list(set(df['game__appid'])))
        df.set_index(['game__appid', 'player__steamid'], inplace=True)
        mat = sparse.coo_matrix((df.playtime_forever, (df.index.labels[0], df.index.labels[1])))
        df = pd.DataFrame(mat.T.toarray(), index=unique_players, columns=unique_games)
        return df

    def compute_svd(self):
        U, s, V = np.linalg.svd(self.df, full_matrices=False)
        S = np.diag(s)
        u=U[:,:self.k]
        v=V[:self.k,:]
        s=S[:self.k,:self.k]

        self.u = u
        self.v = v
        self.s = s

        self.out = []
        for i in range(self.df.shape[1]):
            self.out.append(np.dot(u[self.player_index], np.dot(s, v.T[i])))
            
        dist = {}
        self.out = np.array(self.out)
        if self.pop:
            dist.update({0: list(reversed(np.argsort(self.out[self.out.nonzero()]))) })
        else:
            rec = self.out[self.out.nonzero()]
            np.random.shuffle(rec)
            dist.update({0: np.argsort(rec)})
        return dist

    def compute_userbased(self):
        df = np.array(self.df)
        self.Y = squareform(pdist(df, 'cosine'))
        self.U = np.argsort(self.Y)[:,1:self.K]
        self.out = np.mean(np.array(df)[self.U], 1)
        dist = {}
        if self.pop:
            dist.update({0: list(reversed(np.argsort(self.out[self.player_index]))) })
        else:
            rec = self.out[self.player_index][self.out[self.player_index].nonzero()]
            np.random.shuffle(rec)
            dist.update({0: np.argsort(rec)})
        return dist

    def compute_itembased(self):
        df_arr = np.array(self.df.T)
        dist = {}
        for k, val in enumerate(self.player_top_games):
            gdist=[]
            for i in df_arr:
                try:
                    gdist.append(cosine(df_arr[self.df.columns.get_loc(val)], i))
                except:
                    pass
            gdist = np.array(gdist)
            if self.pop:
                dist.update({k: np.argsort(gdist[gdist.nonzero()])})
            else:
                gdist = gdist[gdist.nonzero()]
                np.random.shuffle(gdist)
                dist.update({k: np.argsort(gdist)})
        return dist

    def compute(self):
        if self.df is None:
            self.df = self.load_dataframe()
        if self.steamid not in self.df.index:
            self.df = self.df.append(self.sparse_matrix(self.player_vector))
            self.player_vector.reset_index(level=['game__appid', 'player__steamid'], inplace=True)
        try:
            self.player_index = self.df.index.get_loc(self.steamid)
        except:
            self.player_index = self.df.index.get_loc(int(self.steamid))
        self.df = self.df.fillna(0)
        #self.df[self.df < self.playtime_threshold] = 0
        #self.df[self.df >= self.playtime_threshold] = 1
        #self.df[self.df > 0] = 1
        self.df = self.df.astype(int)
        if self.alg == 'svd' or self.alg == 'auto':
            self.rec_vector = self.get_clean_recs(compute_svd())
        if self.alg == 'ub':
            self.rec_vector = self.get_clean_recs(compute_userbased())
        elif self.alg == 'ib':
            self.rec_vector = self.get_clean_recs(compute_itembased())
        return self.rec_vector

    def get_clean_recs(self, rec_dict):
        player_tags = pd.DataFrame([self.get_player_top_tags()])
        player_tags[player_tags > 0] = 1
        self.dict = rec_dict
        if self.pop:
            games = DataFrameManager.get_good_games()
        else:
            games = DataFrameManager.get_not_pop_good_games()

        indexes = []
        for i in games:
            try:
                indexes.append(self.df.columns.get_loc(i))
            except:
                pass
        print('LEN IDX:', len(indexes))
        clean_recs = []
        i = 0
        for i in range(len(list(rec_dict.values())[0])):
            for j in list(rec_dict.keys()):
                try:
                    
                    if rec_dict[j] is not None:
                        if self.df.columns[rec_dict[j][i]] not in self.owned_games \
                        and rec_dict[j][i] not in clean_recs and rec_dict[j][i] in indexes: 
                            clean_recs.append(rec_dict[j][i])
                except Exception as ex:
                    print(ex)
                    pass
                i += 1
                if len(clean_recs) == 12:
                    return clean_recs
        return clean_recs
    
    def get_player_top_tags(self, top_n=3):
        top_tags = {}
        for i in self.player_top_games:
            tags = GameTag.objects.filter(game__appid=i).values('tag__name', 'votes')
            for j in tags:
                if j['tag__name'] in top_tags.keys():
                    top_tags[j['tag__name']] += j['votes']
                elif j['tag__name'] != 'Action':
                    top_tags.update({ j['tag__name']: j['votes'] })
        return dict(sorted(top_tags.items(), key=operator.itemgetter(1), reverse=True)[:top_n])
    
    def get_game_top_tags(self, appid, top_n=3):
        top_tags = {}
        tags = GameTag.objects.filter(game__appid=appid).values('tag__name', 'votes')
        for j in tags:
            if j['tag__name'] in top_tags.keys():
                top_tags[j['tag__name']] += j['votes']
            elif j['tag__name'] != 'Action':
                top_tags.update({ j['tag__name']: j['votes'] })
        return dict(sorted(top_tags.items(), key=operator.itemgetter(1), reverse=True)[:top_n])
    
    @staticmethod
    def get_idx_threshold(data, thr):
        a = 0
        for i in sorted(data):
            if i >= thr:
                break
            a += 1
        return a

    def load_dataframe(self):
        #dfm = DataFrameManager()
        df = DataFrameManager.get_dataframe()
        #df = DataFrameManager.get_dataframe()
        #df = self.sparse_matrix(df)
        pg = []
        for i in list(self.player_vector['game__appid']):
            if i not in df['game__appid']:
                pg += list(PlayerGame.objects.filter(game__appid=i).values('player__steamid','game__appid','playtime_forever'))
        pg = pd.DataFrame(pg)
        df = pd.concat([df,pg])
        df = self.sparse_matrix(df)

        rnd_users = random.sample(range(1, df.shape[0] - 1), 100)
        df = df.loc[df.index[rnd_users]] #.loc[player_ids]
        if self.pop:
            games = DataFrameManager.get_good_games()
        else:
            games = DataFrameManager.get_not_pop_good_games()
        indexes = []

        for i in games:
            try:
                indexes.append(df.columns.get_loc(i))
            except:
                pass
        df = df.T.loc[df.T.index[indexes]].T
        return df
    
    def load_playertag(self):
        df = DataFrameManager.get_playertag_df()
        if str(self.steamid) not in list(df['player__steamid']):
            player_tag = DataFrameManager.get_player_tag([self.steamid], topgames=self.player_vector['game__appid'])
            pdf = pd.DataFrame(player_tag)
            df = pd.concat([df, pdf])
        
        unique_players = sorted(list(set(df['player__steamid'])))
        unique_games = sorted(list(set(df['game__tag'])))
        df.set_index(['game__tag', 'player__steamid'], inplace=True)
        mat = sparse.coo_matrix((df.votes, (df.index.labels[0], df.index.labels[1])))
        df = pd.DataFrame(mat.T.toarray(), index=unique_players, columns=unique_games)

        rnd_users = random.sample(range(1, df.shape[0] - 1), 100)
        player_idx = df.index.get_loc(self.steamid)
        if player_idx not in rnd_users:
            rnd_users.append(player_idx)
        df = df.loc[df.index[rnd_users]] #.loc[player_ids]
        return df

    @update_checker
    def recommend(self, N=20):
        timestart = time.time()
        if self.df is None:
            self.df = self.load_dataframe()
        self.compute()
       # print('FINAL FILTRATION: ', time.time() - timestart)
        
        self.games = []
        try:
            for i in self.rec_vector:
                self.games.append(self.df.columns[i])
        except Exception as ex:
            print(ex)
        game_objects = []
        for i in self.games:
            try:
                game_objects.append(Game.objects.filter(appid=i)[0])
            except:
                #print('Appid does not exists:', i)
                pass
        games = game_objects
        print(self.df.shape)
        print('TIME LEFT: ', time.time() - timestart)
        return {'steamid': self.steamid, 'games': games}


class Filter:
    def setdiff(arr1, arr2):
        clean_arr = []        
        for i in arr1:
            if i not in arr2:
                clean_arr.append(i)
            if len(clean_arr) == 12:
                break
        return clean_arr
        
    def intersect(arr1, arr2):
        clean_arr = []        
        for i in arr1:
            if i in arr2:
                clean_arr.append(i)
        return clean_arr

# Luther 76561197998199645
# Kaminekich 76561198021670050
# 76561198129826391

#timestart = time.time()
#print('run time: ', time.time() - timestart)